# ![FONT](https://readme-typing-svg.demolab.com?font=Schoolbell&pause=3000&size=40&random=false&repeat=false&color=343A40&width=85&height=70&lines=BEST) ![FONT](https://readme-typing-svg.demolab.com?font=Cabin+Sketch&pause=3000&size=40&random=false&repeat=false&color=343A40&width=160&height=70&lines=GOOGLE) ![FONT](https://readme-typing-svg.demolab.com?font=Aladin&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=FONTS)  

## ![FONT](https://readme-typing-svg.demolab.com?font=Permanent+Marker&pause=3000&size=30&random=false&repeat=false&color=343A40&width=435&height=70&lines=IMPLEMENTER+:)

- index.html:
```html
<!DOCTYPE html>
    <html>
        <head>
            <link rel="stylesheet" href="./style.css">
        </head>
    </html>
```

- style.css:  

> Une seule font via la balise
```css
@import url('https://fonts.googleapis.com/css2?family=Grand+Hotel&display=swap');

p {
    font-family: 'Grand Hotel';
}
```

> Plusieurs fonts via la classe ou l'id
```css
@import url('https://fonts.googleapis.com/css2?family=Grand+Hotel&family=Orbitron&display=swap');

.classe {
    font-family: 'Grand Hotel';
}

#id {
    font-family: 'Orbitron';
}
```

## ![FONT](https://readme-typing-svg.demolab.com?font=Permanent+Marker&pause=3000&size=30&random=false&repeat=false&color=343A40&width=435&height=70&lines=LISTE+:)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Orbitron&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=ORBITRON)](https://fonts.google.com/specimen/Orbitron)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Anton&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=ANTON)](https://fonts.google.com/specimen/Anton)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Aladin&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=ALADIN)](https://fonts.google.com/specimen/Aladin)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Rye&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=RYE)](https://fonts.google.com/specimen/Rye)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Grand+Hotel&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=GRAND+HOTEL)](https://fonts.google.com/specimen/Grand+Hotel)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Schoolbell&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=SCHOOLBELL)](https://fonts.google.com/specimen/Schoolbell)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Pirata+One&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=PIRATA+ONE)](https://fonts.google.com/specimen/Pirata+One)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Cabin+Sketch&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=CABIN+SKETCH)](https://fonts.google.com/specimen/Cabin+Sketch)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Irish+Grover&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=IRISH+GROVER)](https://fonts.google.com/specimen/Irish+Grover)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Bowlby+One+SC&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=BOWLBY+ONE+SC)](https://fonts.google.com/specimen/Bowlby+One+SC)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Henny+Penny&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=HENNY+PENNY)](https://fonts.google.com/specimen/Henny+Penny)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Permanent+Marker&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=PERMANENT+MARKER)](https://fonts.google.com/specimen/Permanent+Marker)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Concert+One&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=CONCERT+ONE)](https://fonts.google.com/specimen/Concert+One)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Rubik+Doodle+Shadow&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=RUBIK+DOODLE+SHADOW)](https://fonts.google.com/specimen/Rubik+Doodle+Shadow)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Monoton&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=MONOTON)](https://fonts.google.com/specimen/Monoton)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Rock+Salt&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=80&lines=ROCK+SALT)](https://fonts.google.com/specimen/Rock+Salt)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Creepster&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=CREEPSTER)](https://fonts.google.com/specimen/Creepster)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Freehand&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=FREEHAND)](https://fonts.google.com/specimen/Freehand)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Metal+Mania&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=METAL+MANIA)](https://fonts.google.com/specimen/Metal+Mania)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Shojumaru&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=SHOJUMARU)](https://fonts.google.com/specimen/Shojumaru)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Emilys+Candy&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=EMILYS+CANDY)](https://fonts.google.com/specimen/Emilys+Candy)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Frijole&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=FRIJOLE)](https://fonts.google.com/specimen/Frijole)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Caesar+Dressing&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=CAESAR+DRESSING)](https://fonts.google.com/specimen/Caesar+Dressing)

#### [![FONT](https://readme-typing-svg.demolab.com?font=Astloch&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=ASTLOCH)](https://fonts.google.com/specimen/Astloch)

#### [![Festive](https://readme-typing-svg.demolab.com?font=Festive&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Festive)](https://fonts.google.com/specimen/Festive)

#### [![Smokum](https://readme-typing-svg.demolab.com?font=Smokum&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Smokum)](https://fonts.google.com/specimen/Smokum)

#### [![Ewert](https://readme-typing-svg.demolab.com?font=Ewert&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Ewert)](https://fonts.google.com/specimen/Ewert)

#### [![Jim Nightshade](https://readme-typing-svg.demolab.com?font=Jim+Nightshade&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Jim+Nightshade)](https://fonts.google.com/specimen/Jim+Nightshade)

#### [![Neonderthaw](https://readme-typing-svg.demolab.com?font=Neonderthaw&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Neonderthaw)](https://fonts.google.com/specimen/Neonderthaw)

#### [![Stalinist One](https://readme-typing-svg.demolab.com?font=Stalinist+One&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Stalinist+One)](https://fonts.google.com/specimen/Stalinist+One)

#### [![Bigelow Rules](https://readme-typing-svg.demolab.com?font=Bigelow+Rules&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Bigelow+Rules)](https://fonts.google.com/specimen/Bigelow+Rules)

#### [![Rubik Glitch](https://readme-typing-svg.demolab.com?font=Rubik+Glitch&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Rubik+Glitch)](https://fonts.google.com/specimen/Rubik+Glitch)

#### [![Bokor](https://readme-typing-svg.demolab.com?font=Bokor&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Bokor)](https://fonts.google.com/specimen/Bokor)

#### [![Alumni Sans Collegiate One](https://readme-typing-svg.demolab.com?font=Alumni+Sans+Collegiate+One&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Alumni+Sans+Collegiate+One)](https://fonts.google.com/specimen/Alumni+Sans+Collegiate+One)

#### [![Rubik Vinyl](https://readme-typing-svg.demolab.com?font=Rubik+Vinyl&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Rubik+Vinyl)](https://fonts.google.com/specimen/Rubik+Vinyl)

#### [![My Soul](https://readme-typing-svg.demolab.com?font=My+Soul&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=My+Soul)](https://fonts.google.com/specimen/My+Soul)  

#### [![Nabla](https://readme-typing-svg.demolab.com?font=Nabla&pause=3000&size=40&random=false&repeat=false&color=343A40&width=435&height=70&lines=Nabla)](https://fonts.google.com/specimen/Nabla)  







